// @dart=2.9
import 'dart:io';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'statchart.dart';
import 'database.dart';
import 'texts.dart';

class TrackSelectPage extends StatefulWidget {
  @override
  _TrackSelectPageState createState() => _TrackSelectPageState();
}

class _TrackSelectPageState extends State<TrackSelectPage> {
  List<Map<String, Object>> _trackList = [];

  void _showChart(int trkId) async {
    List<Map<String, Object>> data =
        await DB.handle.query('WayPoint', where: 'track=$trkId');

    Navigator.push(
        context, new MaterialPageRoute(builder: (context) => StatChart(data)));
  }

  void _deleteTrack(int trkId) async {
    await DB.handle.delete('WayPoint', where: 'track=$trkId');
    await DB.handle.delete('track', where: 'id=$trkId');

    setState(() {
      _trackList = List.from(_trackList)..clear();
      _loadTracks();
    });
  }

  Future<void> _saveTrack(int trkId, String trackName) async {
    var status = await Permission.storage.status;
    if (!status.isGranted) {
      await Permission.storage.request();
    }
    Directory("/storage/emulated/0/Download" + '/MarschRide').create();
    var filePath =
        "/storage/emulated/0/Download/MarschRide" + '/$trackName.gpx';
    final file = File(filePath);

    String content = '''<?xml version="1.0" encoding="UTF-8"?>
    <gpx
      version="1.1"
      creator="MarschRide"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns="http://www.topografix.com/GPX/1/1"
      xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd"
      xmlns:gpxtpx="http://www.garmin.com/xmlschemas/TrackPointExtension/v1">\n''';

    content += '''<trk>
                  <name>$trackName</name>
                  <trkseg>\n''';

    await file.writeAsString(content);

    content = "";
    int offset = 1000;
    int count = 0;
    while (true) {
      print("QUERY LIMIT $offset OFFSET $count");
      List<Map<String, Object>> data = await DB.handle.rawQuery(
          "SELECT * FROM WayPoint WHERE track=$trkId LIMIT $offset OFFSET $count");
      count += offset;
      print("QUERY END");

      StringBuffer buffer = StringBuffer();
      content = "";

      for (Map<String, Object> line in data) {
        String tme = DateTime.parse(line['time']).toString().substring(0, 23);
        tme += 'Z';
        tme = tme.replaceAll(' ', 'T');
        content += '''<trkpt lat="${line['lat'].toString()}"
        lon="${line['lon'].toString()}">
        <ele>${line['ele'].toString()}</ele>
        <time>$tme</time>
        <extensions>''';

        if (line['cadence'] != 0) {
          content += "<cadence>${line['cadence'].toString()}</cadence>";
        }

        if (line['pulse'] != 0) {
          content += "<heartrate>${line['pulse'].toString()}</heartrate>";
        }

        if (line['power'] != 0) {
          content += "<power>${line['power'].toString()}</power>";
        }

        content += '''</extensions>
        </trkpt>\n''';

        buffer.write(content);
        content = "";
      }

      print("STRING END");

      print("WRITE START");

      await file.writeAsString(buffer.toString(), mode: FileMode.append);

      print("WRITE END");

      content = "";

      setState(() {}); // update progress

      if (data.length < offset) {
        break;
      }
    }
    content += "</trkseg></trk></gpx>\n";

    await file.writeAsString(content, mode: FileMode.append);
  }

  void _loadTracks() async {
    _trackList = await DB.handle.query('track', orderBy: "id DESC");
    setState(() {});
  }

  @override
  void initState() {
    _loadTracks();
    super.initState();
  }

  void deleteDialog(int index, BuildContext context) {
    // set up the buttons
    Widget cancelButton = ElevatedButton(
      child: Text(Texts.no()),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    Widget continueButton = ElevatedButton(
      child: Text(Texts.yes()),
      onPressed: () {
        _deleteTrack(_trackList[index]['id']);
        Navigator.of(context).pop();
      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(Texts.deleteTour()),
      content: Text(Texts.shallDeleteTour()),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void message(BuildContext context, String title, String msg) {
    // set up the button
    Widget okButton = ElevatedButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(title),
      content: Text(msg),
      actions: [
        okButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void showProgressDialog(BuildContext context) {
    AlertDialog alert = AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(),
          Container(
              margin: EdgeInsets.only(left: 7), child: Text(Texts.saving())),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void handleSave(BuildContext context, int trackId, String trackName) async {
    showProgressDialog(context);
    await _saveTrack(trackId, trackName);
    Navigator.pop(context);
    message(context, Texts.saved(), trackName + " " + Texts.isSaved());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(Texts.tours()),
          leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.black),
              onPressed: () => Navigator.pop(context)),
        ),
        body: ListView.builder(
            itemCount: _trackList.length,
            itemBuilder: (BuildContext ctxt, int index) {
              String trackName =
                  _trackList[index]['name'].toString().split('\n').elementAt(0);
              String trackDetails = _trackList[index]['name']
                  .toString()
                  .replaceAll(trackName, '');

              return ListTile(
                leading: Icon(Icons.show_chart),
                title: Text(trackName),
                subtitle: Text(trackDetails),
                onTap: () {
                  _showChart(_trackList[index]['id']);
                },
                onLongPress: () {
                  deleteDialog(index, context);
                },
                trailing: IconButton(
                  icon: Icon(Icons.save_alt),
                  onPressed: () {
                    handleSave(context, _trackList[index]['id'], trackName);
                  },
                ),
              );
            }));
  }
}
