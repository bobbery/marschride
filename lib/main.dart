// @dart=2.9

import 'dart:async';
import 'dart:io';

import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter/material.dart';
import 'package:wakelock/wakelock.dart';
import 'package:latlong2/latlong.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:splashscreen/splashscreen.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_map_location/flutter_map_location.dart';
import 'package:geolocator/geolocator.dart' as gloc;
import 'package:sqflite/sqflite.dart';
import 'package:flutter_beep/flutter_beep.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:keep_screen_on/keep_screen_on.dart';

import 'sensors/sensortypewidget.dart';
import 'sensors/globalsensordata.dart';

import 'valuewidget.dart';
import 'toleranttileprovider.dart';
import 'maploaderpage.dart';
import 'database.dart';
import 'trackselect.dart';
import 'texts.dart';

void main() {
  KeepScreenOn.turnOn();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual,
        overlays: [SystemUiOverlay.bottom, SystemUiOverlay.top]);
    return MaterialApp(
      title: 'MarschRide',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: Splash(),
      debugShowCheckedModeBanner: false,
      locale: Locale('de', 'DE'),
    );
  }
}

class Splash extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SplashScreen(
      seconds: 3,
      navigateAfterSeconds: MyHomePage(),
      title: Text(
        'MarschRide',
        textScaleFactor: 4,
      ),
      image: Image(image: AssetImage('img/dir_bike.png')),
      loadingText: Text(Texts.loading()),
      photoSize: 50.0,
      loaderColor: Colors.black,
      backgroundColor: Colors.lightGreen,
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage() : title = 'MarschRide';

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _speed = 0;
  LatLng _curPos = LatLng(0.0, 0.0);
  String _offlineMapPath;
  bool _recording = false;
  double _distance;
  LatLng _oldPos = LatLng(0.0, 0.0);
  int _trackNum = 0;
  int _pageNum = 0;
  String strDur = "00:00:00";
  String strDist = "0000.00";
  String strDistTotal = "0000.00";
  double _distTotal = 0.0;
  String strAvg = "0.0";
  bool _stopped = true;
  Duration _tripDuration = Duration(seconds: 0);
  String trackName = "Kein Name";
  String _timeString = "00:00";
  double _lastHeight = 0.0;
  //Location location;
  StreamSubscription _backgroundLocSubscription;
  bool _reloadTiles = false;
  DateTime _lastTimeUpdate = DateTime.now();

  MapController mapController = MapController();
  final List<Marker> userLocationMarkers = <Marker>[];

  void _findOfflineMapPath() async {
    final Directory extDir = await getApplicationDocumentsDirectory();
    if (!mounted) return;
    setState(() {
      _offlineMapPath = extDir.path + "/offlineMap";
      print(_offlineMapPath);
    });
  }

  void _openDb() async {
    DB.handle = await openDatabase('ride.db', version: 1,
        onCreate: (Database db, int version) async {
      await db.execute('CREATE TABLE track ( '
          'id INTEGER PRIMARY KEY AUTOINCREMENT, '
          'name TEXT)');
      await db.execute('CREATE TABLE WayPoint ( '
          'id INTEGER PRIMARY KEY, '
          'track INTEGER, '
          'time DATETIME, '
          'lat REAL, lon REAL, '
          'ele INTEGER, '
          'speed INTEGER, '
          'pulse INTEGER, '
          'cadence INTEGER, '
          'power INTEGER, '
          'FOREIGN KEY(track) REFERENCES track(id)'
          ')');
      await db.execute('CREATE TABLE totals ( '
          'id INTEGER PRIMARY KEY, '
          'totalDistance REAL)');
      var vals1 = Map<String, Object>();
      vals1['id'] = 1;
      vals1['totalDistance'] = _distTotal;
      await db.insert('totals', vals1,
          conflictAlgorithm: ConflictAlgorithm.replace);
    });

    var vals = await DB.handle.query('totals', where: 'id=1');

    setState(() {
      _distTotal = vals[0]['totalDistance'] as double;
      strDistTotal = (_distTotal / 1000.0).toStringAsFixed(2).padLeft(7, '0');
    });
  }

  void startBackgroundLocation() async {}

  @override
  void initState() {
    super.initState();

    Future.delayed(Duration.zero, () {
      legalizeBackgroundLocation(context);
    });

    Timer.periodic(Duration(seconds: 1), (timer) {
      setState(() {
        _timeString = DateFormat('HH:mm').format(DateTime.now());
        if (_recording &&
            _lastTimeUpdate.difference(DateTime.now()).inSeconds > 3) {
          _tripDuration += DateTime.now().difference(_lastTimeUpdate);
        } else if (_recording) {
          _tripDuration += Duration(seconds: 1);
        }
        _lastTimeUpdate = DateTime.now();
        strDur = _tripDuration.toString();
        strDur = strDur.substring(0, strDur.length - 7).padLeft(8, '0');
      });
    });

    _restoreLang();

    initializeDateFormatting();
    _findOfflineMapPath();
    Wakelock.enable();

    _openDb();

    //_listenToBackgroundLocation();
  }

  void _updatePosition() async {
    gloc.Position location = await gloc.Geolocator.getCurrentPosition();
    setState(() {
      _speed = (location.speed * 3.6).toInt();
      GlobalSensorData.speed = _speed;
      GlobalSensorData.ele = location.altitude.toInt();

      double dist = gloc.Geolocator.distanceBetween(_curPos.latitude,
          _curPos.longitude, location.latitude, location.longitude);

      if (_curPos == LatLng(0, 0)) {
        dist = 0.0;
      }

      if (_distance == null) {
        _distance = 0.0;
      }

      if (dist > 3.0 && _recording) {
        _distance += dist;
        _distTotal += dist;
        strDistTotal = (_distTotal / 1000.0).toStringAsFixed(2).padLeft(7, '0');
        strDist = (_distance / 1000.0).toStringAsFixed(2).padLeft(7, '0');
        strAvg = ((_distance / 1000.0) / (_tripDuration.inSeconds / 3600))
            .toStringAsFixed(2);

        GlobalSensorData.slope =
            (100.0 * (location.altitude - _lastHeight) ~/ dist).toInt();
      }
      _curPos = LatLng(location.latitude, location.longitude);
      _lastHeight = location.altitude;

      print("New location ${location.toString()}");
      storeLocation(_curPos);
    });
  }

  Widget _space(double screenFactor) => Container(
      height: screenFactor * MediaQuery.of(context).size.height,
      child: Opacity(
        opacity: 0.5,
      ));

  List<Widget> _page0() {
    return [
      ValueWidget(Texts.speed(), _speed.toString().padLeft(3, '0'), ""),
      ValueWidget(Texts.pulse(), "---", "pulse"),
      _space(0.3),
      _space(0.3),
      ValueWidget(Texts.cadence(), "---", "cadence"),
      ValueWidget(Texts.power(), "---", "power"),
    ];
  }

  List<Widget> _page1() {
    return [
      ValueWidget(Texts.trip(), strDist, ""),
      _space(0.4),
      ValueWidget(Texts.time(), strDur, ""),
      _space(0.3),
    ];
  }

  List<Widget> _page2() {
    return [
      Row(
        children: [
          Spacer(),
          ValueWidget(Texts.ele(),
              GlobalSensorData.ele.toStringAsFixed(0).padLeft(3), ""),
          Spacer(),
        ],
      ),
      _space(0.4),
      ValueWidget(Texts.kmTotal(), strDistTotal, ""),
      _space(0.3),
    ];
  }

  Widget _buildStack() => Stack(
        alignment: AlignmentDirectional.topCenter,
        children: [
          _offlineMapPath != null ? buildMap(context) : Text("OHOH"),
          Opacity(
              opacity: _pageNum == 2 ? 1.0 : 0.0,
              child: ListView(
                padding: const EdgeInsets.all(4),
                children: _page2(),
              )),
          Opacity(
              opacity: _pageNum == 1 ? 1.0 : 0.0,
              child: ListView(
                padding: const EdgeInsets.all(4),
                children: _page1(),
              )),
          Opacity(
              opacity: _pageNum == 0 ? 1.0 : 0.0,
              child: GridView.count(
                crossAxisCount: 2,
                padding: const EdgeInsets.all(4),
                children: _page0(),
              )),
        ],
      );

  void newTrack() async {
    var vals = Map<String, Object>();
    vals['name'] = DateFormat('EEEE, d. MMM, yyyy – HH:mm:ss', 'de')
        .format(DateTime.now());
    DB.handle.insert('track', vals);
    var result = await DB.handle.rawQuery("SELECT MAX(id) FROM track");
    _trackNum = result[0]['MAX(id)'];
  }

  Future<void> _getTrackName(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(Texts.tourName()),
            content: TextField(
              onChanged: (value) {
                setState(() {
                  trackName = value;
                });
              },
              decoration: InputDecoration(hintText: Texts.noName()),
            ),
            actions: <Widget>[
              ElevatedButton(
                child: Text('OK'),
                onPressed: () {
                  setState(() {
                    Navigator.pop(context);
                  });
                },
              ),
            ],
          );
        });
  }

  void _saveTrack() async {
    await _getTrackName(context);
    try {
      updateTrackDescription(); // update old track if exist
    } catch (e) {}
  }

  void abortRecordingDialog(BuildContext context) {
    // set up the buttons
    Widget cancelButton = ElevatedButton(
      child: Text(Texts.no()),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    Widget continueButton = ElevatedButton(
      child: Text(Texts.yes()),
      onPressed: () {
        Navigator.of(context).pop();
        _saveTrack();
        _distance = 0.0;
        _recording = false;
        _stopped = true;
      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(Texts.endTour()),
      content: Text(Texts.endTourText()),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void goToMapLoaderPage() async {
    await Navigator.push(context,
        new MaterialPageRoute(builder: (context) => MapLoaderPage(_curPos)));
    setState(() {
      _reloadTiles = true;
    });
  }

  void _saveLang() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('lang', Texts.lang);
  }

  void _restoreLang() async {
    final prefs = await SharedPreferences.getInstance();
    Texts.lang = prefs.getString('lang');
  }

  void legalizeBackgroundLocation(BuildContext context) async {
    if (await Permission.locationAlways.status != PermissionStatus.granted) {
      await showDialog(
          context: context,
          builder: (BuildContext context) => AlertDialog(
                title: Text('Location in Background'),
                content: Text(
                    'This app does collect location data, that is used to record tour-data in form of GPX-Tracks, even when the app is closed or not in use. All recorded data is stored on the device locally only and can be deleted at any time by the user.'),
                actions: <Widget>[
                  ElevatedButton(
                    child: Text('Turn off'),
                    onPressed: () => Navigator.pop(context),
                  ),
                  ElevatedButton(
                    child: Text('Turn on'),
                    onPressed: () {
                      Navigator.pop(context);
                      Permission.locationAlways
                          .request()
                          .then((value) => startBackgroundLocation());
                    },
                  ),
                ],
              ));
    } else {
      Permission.locationAlways
          .request()
          .then((value) => startBackgroundLocation());
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: _onExit,
        child: Scaffold(
          appBar: AppBar(
            title: Text(widget.title + " - " + _timeString),
          ),
          floatingActionButton: SizedBox(
            width: 80.0,
            height: 80.0,
            child: FloatingActionButton(
              onPressed: () {
                setState(() {
                  if (!_recording && !_stopped) {
                    _recording = true;
                  } else {
                    _recording = false;
                  }
                });
              },
              isExtended: true,
              tooltip: Texts.ttStartStop(),
              child: GestureDetector(
                  onLongPress: () {
                    if (_stopped) {
                      setState(() {
                        //_tripDuration = Duration(seconds: 0);
                        newTrack();
                        _stopped = false;
                        _recording = true;
                        strDist = "0000.00";
                      });
                    } else {
                      setState(() {
                        abortRecordingDialog(context);
                      });
                    }
                  },
                  child: _recording
                      ? Icon(
                          Icons.pause_circle_filled,
                          color: Colors.black,
                          size: 80,
                        )
                      : Icon(
                          Icons.play_circle_fill,
                          color: Colors.black,
                          size: 80,
                        )),
              backgroundColor: _recording || !_stopped
                  ? Colors.redAccent
                  : Colors.lightGreenAccent,
            ),
          ),
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerDocked,
          bottomNavigationBar: BottomAppBar(
            color: Colors.lightGreen,
            child: Container(height: 50.0),
          ),
          drawer: Drawer(
            child: ListView(
              padding: EdgeInsets.zero,
              children: <Widget>[
                DrawerHeader(
                  decoration: BoxDecoration(color: Colors.lightGreen),
                  child: Icon(
                    Icons.directions_bike,
                    size: 100.0,
                  ),
                ),
                ListTile(
                  leading: Icon(Icons.map),
                  title: Text(Texts.map()),
                  onTap: () {
                    goToMapLoaderPage();
                  },
                ),
                ListTile(
                  leading: Icon(Icons.wifi_tethering),
                  title: Text(Texts.sensors()),
                  onTap: () {
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) => SensorTypeWidget()));
                  },
                ),
                ListTile(
                  leading: Icon(Icons.show_chart),
                  title: Text(Texts.tours()),
                  onTap: () {
                    _showChart();
                  },
                ),
                ListTile(
                  leading: Icon(Icons.language),
                  title: Text(Texts.theLanguage()),
                  onTap: () {
                    setState(() {
                      if (Texts.lang == "DE") {
                        Texts.lang = "EN";
                      } else {
                        Texts.lang = "DE";
                      }
                      _saveLang();
                    });
                  },
                ),
                AboutListTile(
                  icon: Icon(Icons.info_outline),
                  child: Text(Texts.about()),
                  applicationName: "Marsch\nRide",
                  applicationIcon:
                      Image(width: 64, image: AssetImage('img/dir_bike.png')),
                  aboutBoxChildren: [
                    Image(image: AssetImage('img/about.jpg')),
                    Divider(),
                    Text("Marsch = Markl und Scholz",
                        textAlign: TextAlign.center),
                    Divider(),
                    Text(
                      "Wir wünschen dir mit der App so viel Spaß wie wir beim Entwickeln hatten",
                      textAlign: TextAlign.center,
                    ),
                    Divider(),
                    Text("Viele Grüße Hans und Stefan",
                        textAlign: TextAlign.center)
                  ],
                ),
                Divider(),
                ListTile(
                  leading: Icon(Icons.power_settings_new),
                  title: Text(Texts.theEnd()),
                  onTap: () {
                    //BackgroundLocation.stopLocationService();
                    exit(0);
                  },
                ),
              ],
            ),
          ),
          body: InkWell(
            child: _buildStack(),
            onTap: () {
              setState(() {
                _pageNum++;
                if (_pageNum > 2) {
                  _pageNum = 0;
                }
              });
            },
          ),
        ));
  }

  Future<bool> _onExit() async {
    List<BluetoothDevice> devices = await FlutterBlue.instance.connectedDevices;

    devices.forEach((device) {
      device.disconnect();
    });
    return Future<bool>.value(true);
  }

  Widget buildMap(BuildContext context) {
    return FlutterMap(
      mapController: mapController,
      options: MapOptions(
        plugins: <MapPlugin>[
          LocationPlugin(),
        ],
        interactiveFlags: InteractiveFlag.all,
      ),
      layers: <LayerOptions>[
        TileLayerOptions(
          retinaMode: _reloadTiles,
          overrideTilesWhenUrlChanges: true,
          tileProvider: TolerantFileTileProvider(),
          placeholderImage: AssetImage('img/no_map.png'),
          urlTemplate: _offlineMapPath + "/{z}/{x}/{y}.png",
        ),
        MarkerLayerOptions(markers: userLocationMarkers),
        LocationOptions(
          (BuildContext context, ValueNotifier<LocationServiceStatus> status,
              Function onPressed) {
            return Text("");
          },
          markerBuilder: myMarkerBuilder,
          onLocationUpdate: (LatLngData ld) {
            print('Location updated: ${ld?.location}');
            mapController?.move(ld.location, 16.0);

            if (_oldPos != ld.location) {
              double angle = -gloc.Geolocator.bearingBetween(
                  _oldPos.latitude,
                  _oldPos.longitude,
                  ld.location.latitude,
                  ld.location.longitude);

              //storeLocation(ld.location);
              _updatePosition();

              print('Angle: $angle');
              mapController?.rotate(angle);
              myHeading = angle;
            }

            _oldPos = ld.location;
          },
          onLocationRequested: (LatLngData ld) {
            if (ld == null || ld.location == null) {
              return;
            }
            mapController?.move(ld.location, 16.0);
          },
        ),
      ],
    );
  }

  void updateTrackDescription() async {
    var vals = Map<String, Object>();

    String name = trackName +
        "\n" +
        DateFormat('EEEE, d. MMM, yyyy – HH:mm:ss', 'de')
            .format(DateTime.now());
    if (Texts.lang == "EN") {
      name = trackName +
          "\n" +
          DateFormat('EEEE, d. MMM, yyyy – HH:mm:ss', 'en')
              .format(DateTime.now());
    }

    name += "\n" + Texts.duration() + ": $strDur";
    name += "\n" + Texts.distance() + ": $strDist km";
    name += "\n" + Texts.avg() + ": $strAvg km/h";

    vals['name'] = name;
    DB.handle.update('track', vals, where: "id = $_trackNum");

    var vals1 = Map<String, Object>();
    vals1['id'] = 1;
    vals1['totalDistance'] = _distTotal;
    await DB.handle
        .insert('totals', vals1, conflictAlgorithm: ConflictAlgorithm.replace);

    _tripDuration = Duration(seconds: 0);
  }

  void storeLocation(LatLng loc) {
    _reloadTiles = false;

    if (!_recording) {
      return;
    }
    print("Storing location");
    var values = Map<String, Object>();

    values['track'] = _trackNum;
    values['time'] = DateTime.now().toIso8601String();
    values['lat'] = loc.latitude;
    values['lon'] = loc.longitude;
    values['ele'] = GlobalSensorData.ele;
    values['speed'] = GlobalSensorData.speed;
    values['pulse'] = GlobalSensorData.heartRate;
    values['cadence'] = GlobalSensorData.cadence;
    values['power'] = GlobalSensorData.power;

    DB.handle.insert('WayPoint', values);
    //FlutterBeep.beep();
  }

  void _showChart() async {
    Navigator.push(context,
        new MaterialPageRoute(builder: (context) => TrackSelectPage()));
  }

  @override
  void dispose() async {
    await DB.handle.close();
    super.dispose();
  }
}
