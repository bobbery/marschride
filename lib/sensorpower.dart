// @dart=2.9
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';

import 'valuewidget.dart';

class SensorPower extends StatefulWidget {
  SensorPower({Key key}) : super(key: key);

  final String title = "Leistungsmesser";
  final FlutterBlue flutterBlue = FlutterBlue.instance;
  final List<BluetoothDevice> devicesList = [];

  @override
  _SensorPowerState createState() => _SensorPowerState();
}

class _SensorPowerState extends State<SensorPower> {
  Guid uuidCyclingPowerService = Guid("00001818-0000-1000-8000-00805f9b34fb");
  Guid uuicCyclingPowerMeasurementChar =
      Guid("00002a63-0000-1000-8000-00805f9b34fb");
  Guid uuidControlPointChar = Guid("00002a66-0000-1000-8000-00805f9b34fb");
  Guid uuidBatteryService = Guid("0000180f-0000-1000-8000-00805f9b34fb");
  Guid uuidBatteryLevelChar = Guid("00002a19-0000-1000-8000-00805f9b34fb");

  List<int> _values = [];
  int _batteryLevel = 0;

  BluetoothDevice _connectedDevice;
  StreamSubscription<List<BluetoothDevice>> _subscr1;
  StreamSubscription<List<ScanResult>> _subscr2;
  StreamSubscription<List<int>> _subscr3;
  StreamSubscription<List<int>> _subscr4;
  bool _connecting = false;
  BluetoothCharacteristic charControlPoint;

  void _addDeviceTolist(final BluetoothDevice device) {
    if (!widget.devicesList.contains(device)) {
      setState(() {
        widget.devicesList.add(device);
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _subscr1 = widget.flutterBlue.connectedDevices
        .asStream()
        .listen((List<BluetoothDevice> devices) {
      for (BluetoothDevice device in devices) {
        _addDeviceTolist(device);
      }
    });
    _subscr2 =
        widget.flutterBlue.scanResults.listen((List<ScanResult> results) {
      for (ScanResult result in results) {
        _addDeviceTolist(result.device);
      }
    });
    widget.flutterBlue.startScan();
  }

  void _connectToDevice(BluetoothDevice device) async {
    try {
      await device.connect(autoConnect: false);
    } catch (_) {
      await device.disconnect();
      setState(() {
        _connecting = false;
      });
      return;
    }

    BluetoothCharacteristic charPower;
    BluetoothCharacteristic charBattery;

    List<BluetoothService> services = await device.discoverServices();
    services.forEach((service) {
      if (service.uuid == uuidCyclingPowerService) {
        service.characteristics.forEach((c) {
          if (c.uuid == uuicCyclingPowerMeasurementChar) {
            charPower = c;
          }
          if (c.uuid == uuidControlPointChar) {
            charControlPoint = c;
          }
        });
      }
      if (service.uuid == uuidBatteryService) {
        service.characteristics.forEach((c) {
          if (c.uuid == uuidBatteryLevelChar) {
            charBattery = c;
          }
        });
      }
    });

    if (charPower != null) {
      await charPower.setNotifyValue(true);
      _connectedDevice = device;
      print("CONNECTED !!!");
      _subscr3 = charPower.value.listen((value) {
        setState(() {
          _values = value;
        });
      });
    }

    if (charBattery != null && _connectedDevice != null) {
      await charBattery.setNotifyValue(true);
      _subscr4 = charBattery.value.listen((value) {
        setState(() {
          if (value.length > 0) {
            _batteryLevel = value[0];
          }
        });
      });
    }
    setState(() {
      _connecting = false;
    });
  }

  ListView _buildListViewOfDevices() {
    List<Container> containers = [];
    for (BluetoothDevice device in widget.devicesList) {
      if (device.name != '')
        containers.add(
          Container(
            height: 50,
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Column(
                    children: <Widget>[
                      Text(device.name == '' ? '(UNBEKANNT)' : device.name),
                      Text(device.id.toString()),
                    ],
                  ),
                ),
                ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all(Colors.lightGreen),
                  ),
                  child: Text(
                    'Verbinden',
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    setState(() {
                      _connecting = true;
                    });
                    widget.flutterBlue.stopScan();
                    _connectToDevice(device);
                  },
                ),
              ],
            ),
          ),
        );
    }

    return ListView(
      padding: const EdgeInsets.all(8),
      children: <Widget>[
        ...containers,
      ],
    );
  }

  Widget _buildPowerDisplay() {
    int power = 0;
    int cadence = 0;
    int force = 0;
    if (_values.length > 5) {
      power = _values[3] * 256 + _values[2];
      cadence = _values[5] * 256 + _values[4];
      force = _values[7] * 256 + _values[6];
    }
    return ListView(
      padding: const EdgeInsets.all(8),
      children: [
        ValueWidget("Leistung", power.toString(), ""),
        ValueWidget("Kadenz", cadence.toString(), ""),
        ValueWidget("Batterie", _batteryLevel.toString(), ""),
        ValueWidget("Kraft", force.toString(), ""),
        ElevatedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(Colors.lightGreen),
          ),
          child: Text(
            'Trennen',
            style: TextStyle(color: Colors.white),
          ),
          onPressed: () {
            setState(() {
              _connectedDevice.disconnect();
              if (_subscr3 != null) _subscr3.cancel();
              if (_subscr4 != null) _subscr4.cancel();
              _connecting = false;
              _connectedDevice = null;
            });
          },
        ),
        ElevatedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(Colors.lightGreen),
          ),
          child: Text(
            'Kalibrieren',
            style: TextStyle(color: Colors.white),
          ),
          onPressed: () {
            setState(() {
              if (charControlPoint != null) {
                charControlPoint.write([12]); // Offset Comp
              }
            });
          },
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
          actions: <Widget>[
            IconButton(
                icon: Icon(Icons.search),
                onPressed: () {
                  widget.flutterBlue.stopScan();
                  setState(() {
                    widget.devicesList.clear();
                  });

                  widget.flutterBlue.startScan();
                })
          ],
        ),
        body: _connectedDevice == null
            ? (_connecting
                ? Center(child: Text("Verbinden ...", textScaleFactor: 3))
                : _buildListViewOfDevices())
            : _buildPowerDisplay(),
      );

  @override
  void dispose() {
    if (_subscr1 != null) _subscr1.cancel();
    if (_subscr2 != null) _subscr2.cancel();
    if (_subscr3 != null) _subscr3.cancel();
    super.dispose();
  }

  @override
  void deactivate() {
    _connectedDevice.disconnect();
    if (_subscr1 != null) _subscr1.cancel();
    if (_subscr2 != null) _subscr2.cancel();
    if (_subscr3 != null) _subscr3.cancel();
    if (_subscr4 != null) _subscr4.cancel();
    _connecting = false;
    _connectedDevice = null;
    super.deactivate();
  }
}
