class Texts {
  static String lang = "DE";

  static String no() {
    if (lang == "DE") {
      return 'NEIN';
    } else {
      return 'NO';
    }
  }

  static String yes() {
    if (lang == "DE") {
      return 'JA';
    } else {
      return 'YES';
    }
  }

  static String loading() {
    if (lang == "DE") {
      return "Lade ...";
    } else {
      return "Loading ...";
    }
  }

  static String tourName() {
    if (lang == "DE") {
      return 'Wie soll die Tour heißen ?';
    } else {
      return 'How shall the tour be named ?';
    }
  }

  static String noName() {
    if (lang == "DE") {
      return 'Kein Name';
    } else {
      return 'no name';
    }
  }

  static String endTour() {
    if (lang == "DE") {
      return 'Tour beenden ?';
    } else {
      return 'end tour';
    }
  }

  static String endTourText() {
    if (lang == "DE") {
      return "Willst du diese Tour beenden ?";
    } else {
      return "Do you want to end this tour ?";
    }
  }

  static String ttStartStop() {
    if (lang == "DE") {
      return 'Start, Pause und Stop (langer Druck)';
    } else {
      return 'Start, Pause and Stop (long press)';
    }
  }

  static String map() {
    if (lang == "DE") {
      return 'Karte';
    } else {
      return 'map';
    }
  }

  static String sensors() {
    if (lang == "DE") {
      return 'Sensoren';
    } else {
      return 'sensors';
    }
  }

  static String tours() {
    if (lang == "DE") {
      return 'Touren';
    } else {
      return 'tours';
    }
  }

  static String theEnd() {
    if (lang == "DE") {
      return 'Ende';
    } else {
      return 'exit';
    }
  }

  static String duration() {
    if (lang == "DE") {
      return 'Dauer';
    } else {
      return 'duration';
    }
  }

  static String distance() {
    if (lang == "DE") {
      return 'Distanz';
    } else {
      return 'distance';
    }
  }

  static String avg() {
    if (lang == "DE") {
      return 'Durchschnitt';
    } else {
      return 'average';
    }
  }

  static String speed() {
    if (lang == "DE") {
      return 'Geschw.';
    } else {
      return 'speed';
    }
  }

  static String cadence() {
    if (lang == "DE") {
      return 'Kadenz';
    } else {
      return 'cadence';
    }
  }

  static String power() {
    if (lang == "DE") {
      return 'Leistung';
    } else {
      return 'power';
    }
  }

  static String pulse() {
    if (lang == "DE") {
      return 'Puls';
    } else {
      return 'pulse';
    }
  }

  static String time() {
    if (lang == "DE") {
      return 'Zeit';
    } else {
      return 'time';
    }
  }

  static String trip() {
    if (lang == "DE") {
      return 'Tageskilometer';
    } else {
      return 'trip';
    }
  }

  static String ele() {
    if (lang == "DE") {
      return 'Höhe';
    } else {
      return 'elevation';
    }
  }

  static String kmTotal() {
    if (lang == "DE") {
      return 'Gesamtkilometer';
    } else {
      return 'total kilometers';
    }
  }

  static String mapLoad() {
    if (lang == "DE") {
      return 'Karte wird geladen';
    } else {
      return 'map is loading';
    }
  }

  static String loadTile() {
    if (lang == "DE") {
      return 'Lade Kachel';
    } else {
      return 'loading tile';
    }
  }

  static String of() {
    if (lang == "DE") {
      return 'von';
    } else {
      return 'of';
    }
  }

  static String tiles() {
    if (lang == "DE") {
      return 'Kacheln';
    } else {
      return 'tiles';
    }
  }

  static String tooManyTiles() {
    if (lang == "DE") {
      return 'Zu viele Kacheln';
    } else {
      return 'too many tiles';
    }
  }

  static String tooManyTilesSelected() {
    if (lang == "DE") {
      return 'Kacheln sind zu viel, kleineren Bereich wählen (<1000)';
    } else {
      return 'tiles are too much - select smaller area (<1000)';
    }
  }

  static List<String> chartHeadings() {
    if (lang == "DE") {
      return ['Leistung', 'Geschwindigkeit', 'Kadenz', 'Puls', 'Höhe'];
    }
    return ['power', 'speed', 'cadence', 'pulse', 'elevation'];
  }

  static String min() {
    if (lang == "DE") {
      return 'Minimalwert';
    } else {
      return 'min value';
    }
  }

  static String max() {
    if (lang == "DE") {
      return 'Maximalwert';
    } else {
      return 'max value';
    }
  }

  static String avgVal() {
    if (lang == "DE") {
      return 'Mittlewert';
    } else {
      return 'avg value';
    }
  }

  static String notAssigned() {
    if (lang == "DE") {
      return 'nicht vergeben';
    } else {
      return 'not assigned';
    }
  }

  static String reset() {
    if (lang == "DE") {
      return 'Zurücksetzen';
    } else {
      return 'reset';
    }
  }

  static String theLanguage() {
    if (lang == "DE") {
      return 'Deutsch';
    } else {
      return 'english';
    }
  }

  static String about() {
    if (lang == "DE") {
      return 'Über';
    } else {
      return 'about';
    }
  }

  static String position() {
    if (lang == "DE") {
      return 'Position';
    } else {
      return 'position';
    }
  }

  static String load() {
    if (lang == "DE") {
      return 'Laden';
    } else {
      return 'load';
    }
  }

  static String show() {
    if (lang == "DE") {
      return 'Zeigen';
    } else {
      return 'show';
    }
  }

  static String deleteTour() {
    if (lang == "DE") {
      return 'Tour löschen ?';
    } else {
      return 'delete tour ?';
    }
  }

  static String shallDeleteTour() {
    if (lang == "DE") {
      return 'Soll diese Tour gelöscht werden ?';
    } else {
      return 'shall this tour be deleted ?';
    }
  }

  static String saved() {
    if (lang == "DE") {
      return 'Gespeichert';
    } else {
      return 'saved';
    }
  }

  static String isSaved() {
    if (lang == "DE") {
      return 'ist gespeichert';
    } else {
      return 'is saved';
    }
  }

  static String saving() {
    if (lang == "DE") {
      return 'Speichern ...';
    } else {
      return 'saving ...';
    }
  }
}
