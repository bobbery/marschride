// @dart=2.9
import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:segment_display/segment_display.dart';
import 'sensors/blesensor.dart';

class ValueWidget extends StatefulWidget {
  final String _name;
  final String _value;
  final String _sensorId;

  @override
  ValueWidget(this._name, this._value, this._sensorId);

  _ValueWidgetState createState() => _ValueWidgetState(_sensorId);
}

class _ValueWidgetState extends State<ValueWidget> {
  BleSensor _bleSensor;
  int _batteryLevel = 0;
  String _value = "0";
  bool _hasBatteryStatus = false;
  BluetoothDeviceState _connectionStatus = BluetoothDeviceState.connecting;
  bool _isStartup = true;

  _ValueWidgetState(String sensorId) : _bleSensor = BleSensor(sensorId);

  void _reconnect() async {
    setState(() {
      _connectionStatus = BluetoothDeviceState.connecting;
      _bleSensor.reconnect();
    });
  }

  @override
  void initState() {
    _value = widget._value;
    if (widget._sensorId != '') {
      _bleSensor.value.listen((value) {
        setState(() {
          _connectionStatus = BluetoothDeviceState.connected;
          _value = value.toString().padLeft(3, '0');
        });
      });
      _bleSensor.battery.listen((value) {
        setState(() {
          _connectionStatus = BluetoothDeviceState.connected;
          _hasBatteryStatus = true;
          _batteryLevel = value;
        });
      });
      _bleSensor.status.listen((value) {
        if (value == BluetoothDeviceState.disconnected) {
          setState(() {
            _hasBatteryStatus = false;
          });
        }
        setState(() {
          _connectionStatus = value;
          if (_isStartup && value == BluetoothDeviceState.disconnected) {
            _connectionStatus = BluetoothDeviceState.connecting;
            _isStartup = false;
          }
        });
      });
    } else {
      setState(() {
        _connectionStatus = BluetoothDeviceState.connected;
      });
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: _buildDisplay(),
      onLongPress: () {
        _reconnect();
      },
    );
  }

  Widget _statusIcon(IconData icon) {
    return Container(
        alignment: Alignment(0.0, 0.0),
        child: Column(
          children: [
            Center(
                child: Text(widget._name,
                    style:
                        TextStyle(fontSize: 20, fontWeight: FontWeight.bold))),
            Icon(icon, size: 64.0),
          ],
        ));
  }

  Widget _buildDisplay() {
    if (widget._sensorId == '') {
      _value = widget._value;
      return _buildDigital();
    }

    if (!_bleSensor.isConfigured && widget._sensorId != '') {
      return _statusIcon(Icons.remove_circle_outline);
    }

    if (_connectionStatus == BluetoothDeviceState.connecting ||
        _connectionStatus == BluetoothDeviceState.disconnecting) {
      return _statusIcon(Icons.bluetooth_searching);
    }

    if (_connectionStatus == BluetoothDeviceState.disconnected) {
      return _statusIcon(Icons.bluetooth_disabled);
    }

    if (widget._sensorId == '') {
      _value = widget._value;
    }

    return _buildDigital();
  }

  int numNonNumber(String val) {
    int nonNumbers = 0;
    for (int rune in val.runes) {
      if (String.fromCharCode(rune) == ':' ||
          String.fromCharCode(rune) == '.') {
        nonNumbers += 1;
      }
    }
    return nonNumbers;
  }

  Widget _buildDigital() {
    final bool showRed = false;

    String text = "";

    for (int i = 0; i < (numNonNumber(_value) / 2); i++) {
      text += ' ';
    }
    text += _value;

    return Container(
        //color: Colors.blueAccent,
        alignment: Alignment(0.0, 0.0),
        child: Column(
          children: [
            Center(
                child: Text(widget._name,
                    style:
                        TextStyle(fontSize: 20, fontWeight: FontWeight.bold))),
            Center(
                child: SevenSegmentDisplay(
              value: text,
              size: 6.0,
              characterCount: text.length,
              backgroundColor: Colors.transparent,
              segmentStyle: DefaultSegmentStyle(
                enabledColor: showRed ? Colors.red : Colors.black,
                disabledColor: Colors.black.withOpacity(0.15),
              ),
            )),
            if (_hasBatteryStatus)
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                Text(_batteryLevel.toString() + " %",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold))
              ]),
          ],
        ));
  }

  @override
  void dispose() {
    _bleSensor.dispose();
    super.dispose();
  }
}
