// @dart=2.9
import 'dart:math';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_location/flutter_map_location.dart';

double myHeading = 0.0;

class TolerantFileTileProvider extends TileProvider {
  @override
  ImageProvider getImage(Coords<num> coords, TileLayerOptions options) {
    if (options.urlTemplate == null) {
      return null;
    }
    File f = File(getTileUrl(coords, options));
    if (f.existsSync()) {
      return FileImage(File(getTileUrl(coords, options)));
    }
    return null;
  }
}

LocationMarkerBuilder myMarkerBuilder =
    (BuildContext context, LatLngData ld, ValueNotifier<double> heading) {
  final double diameter = ld != null && ld.highAccuracy() ? 60.0 : 120.0;
  return Marker(
    point: ld.location,
    builder: (_) => MyLocationMarker(ld: ld, heading: heading),
    height: diameter,
    width: diameter,
  );
};

double _degree2Radian(double degree) {
  return degree * pi / 180.0;
}

class MyLocationMarker extends StatelessWidget {
  const MyLocationMarker({Key key, this.ld, this.heading}) : super(key: key);

  static final CustomPainter headingerPainter = MyLocationMarkerHeading();
  final LatLngData ld;
  final ValueNotifier<double> heading;

  @override
  Widget build(BuildContext context) {
    final double diameter = ld != null && ld.highAccuracy() ? 22.0 : 80.0;
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Stack(
            alignment: AlignmentDirectional.center,
            children: <Widget>[
              ValueListenableBuilder<double>(
                  valueListenable: heading ?? ValueNotifier<double>(null),
                  builder: (BuildContext context, double value, Widget child) {
                    if (value == null) {
                      return Container();
                    }
                    // Only display heading for an accurate location.
                    if (ld == null || !ld.highAccuracy()) {
                      return Container();
                    }
                    return Transform.rotate(
                      angle: _degree2Radian(-myHeading),
                      child: CustomPaint(
                        painter: headingerPainter,
                      ),
                    );
                  }),
              Container(
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.red[300].withOpacity(0.7)),
                height: diameter,
                width: diameter,
              ),
              Container(
                decoration: const BoxDecoration(
                    shape: BoxShape.circle, color: Colors.green),
                height: 14.0,
                width: 14.0,
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class MyLocationMarkerHeading extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final Rect rect = Rect.fromCircle(
      center: Offset.zero,
      radius: 50,
    );
    final Gradient gradient = RadialGradient(
      colors: <Color>[
        Colors.red.shade500.withOpacity(0.7),
        Colors.red.shade500.withOpacity(0.3),
        Colors.transparent,
      ],
      stops: const <double>[
        0.0,
        0.5,
        1.0,
      ],
    );
    final Paint paint = Paint();
    paint.shader = gradient.createShader(rect);
    canvas.drawArc(rect, _degree2Radian(200), _degree2Radian(140), true, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
