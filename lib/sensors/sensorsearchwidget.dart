// @dart=2.9
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:flutter_blue/gen/flutterblue.pb.dart' as protos;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:device_info/device_info.dart';

class SensorSearchWidget extends StatefulWidget {
  SensorSearchWidget(String id, String title, String guid, Icon icon, {Key key})
      : _id = id,
        _title = title,
        _guid = guid,
        _icon = icon,
        super(key: key);

  final String _id;
  final String _title;
  final String _guid;
  final Icon _icon;
  final FlutterBlue flutterBlue = FlutterBlue.instance;
  final List<BluetoothDevice> devicesList = [];
  @override
  _SensorSearchWidgetState createState() => _SensorSearchWidgetState();
}

class _SensorSearchWidgetState extends State<SensorSearchWidget> {
  StreamSubscription<List<ScanResult>> _subscr1;

  void _addDeviceTolist(final BluetoothDevice device) {
    if (!widget.devicesList.contains(device)) {
      setState(() {
        widget.devicesList.add(device);
      });
    }
  }

  @override
  void initState() {
    super.initState();

    _subscr1 =
        widget.flutterBlue.scanResults.listen((List<ScanResult> results) async {
      DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
      final androidInfo = await deviceInfoPlugin.androidInfo;
      for (ScanResult result in results) {
        print(result.advertisementData.serviceUuids.toString());
        /*if (androidInfo.version.sdkInt > 23) {
          if (result.advertisementData.serviceUuids.contains(widget._guid)) {
            _addDeviceTolist(result.device);
          }
        } else {*/
        _addDeviceTolist(result.device);
        //}
      }
    });
    widget.flutterBlue.startScan();
  }

  ListView _buildListViewOfDevices() {
    List<ListTile> containers = [];
    for (BluetoothDevice device in widget.devicesList) {
      if (device.name != '') {
        containers.add(
          ListTile(
            leading: widget._icon,
            title: Text(device.name),
            subtitle: Text(device.id.toString()),
            onTap: () {
              widget.flutterBlue.stopScan();
              _saveState(device);
              Navigator.pop(context);
            },
          ),
        );
      }
    }
    return ListView(
      padding: const EdgeInsets.all(8),
      children: <Widget>[
        ...containers,
      ],
    );
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text(widget._title),
          actions: <Widget>[
            IconButton(
                icon: Icon(Icons.search),
                onPressed: () async {
                  await widget.flutterBlue.stopScan();
                  setState(() {
                    widget.devicesList.clear();
                  });

                  widget.flutterBlue.startScan();
                })
          ],
        ),
        body: _buildListViewOfDevices(),
      );

  void _saveState(BluetoothDevice device) async {
    final prefs = await SharedPreferences.getInstance();
    if (device != null) {
      protos.BluetoothDevice proto = protos.BluetoothDevice.create()
        ..name = device.name
        ..type = protos.BluetoothDevice_Type.values[device.type.index]
        ..remoteId = device.id.toString();
      prefs.setString(widget._id + '_device', proto.writeToJson());
    } else {
      prefs.setString(widget._id + '_device', null);
    }
  }

  @override
  void dispose() {
    widget.flutterBlue.stopScan();
    if (_subscr1 != null) _subscr1.cancel();
    super.dispose();
  }

  @override
  void deactivate() {
    widget.flutterBlue.stopScan();
    if (_subscr1 != null) _subscr1.cancel();
    super.deactivate();
  }
}
