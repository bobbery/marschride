// @dart=2.9
import 'package:flutter/material.dart';

import 'sensorsearchwidget.dart';
import 'gattconstants.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:flutter_blue/gen/flutterblue.pb.dart' as protos;
import 'package:shared_preferences/shared_preferences.dart';

import 'package:marschride/texts.dart';

class SensorTypeWidget extends StatefulWidget {
  @override
  _SensorTypeWidgetState createState() => _SensorTypeWidgetState();
}

class _SensorTypeWidgetState extends State<SensorTypeWidget> {
  String _pulseName = Texts.notAssigned();
  String _cadenceName = Texts.notAssigned();
  String _powerName = Texts.notAssigned();

  Future<String> _loadState(String id) async {
    BluetoothDevice device;
    final prefs = await SharedPreferences.getInstance();
    String deviceData = prefs.getString(id + '_device');
    if (deviceData != null && deviceData != '') {
      device = BluetoothDevice.fromProto(
          protos.BluetoothDevice.fromJson(deviceData));
      return device.name;
    }
    return Texts.notAssigned();
  }

  @override
  Widget build(BuildContext context) {
    _loadState('pulse').then((value) {
      setState(() {
        _pulseName = value;
      });
    });
    _loadState('cadence').then((value) {
      setState(() {
        _cadenceName = value;
      });
    });
    _loadState('power').then((value) {
      setState(() {
        _powerName = value;
      });
    });

    return Scaffold(
        appBar: AppBar(
          title: Text(Texts.sensors()),
          leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.black),
              onPressed: () => Navigator.of(context).pop()),
        ),
        body: ListView(
          padding: const EdgeInsets.all(8),
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.favorite),
              title: Text(Texts.pulse()),
              subtitle: Text(_pulseName),
              onLongPress: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => SensorSearchWidget(
                            'pulse',
                            Texts.pulse(),
                            UUID_HEART_RATE_SERVICE,
                            Icon(Icons.favorite))));
              },
            ),
            ListTile(
              leading: Icon(Icons.loop),
              title: Text(Texts.cadence()),
              subtitle: Text(_cadenceName),
              onLongPress: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => SensorSearchWidget(
                            'cadence',
                            Texts.cadence(),
                            UUID_CYCLING_SPEED_AND_CADENCE,
                            Icon(Icons.loop))));
              },
            ),
            ListTile(
              leading: Icon(Icons.flash_on),
              title: Text(Texts.power()),
              subtitle: Text(_powerName),
              onLongPress: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => SensorSearchWidget(
                            'power',
                            Texts.power(),
                            UUID_CYCLING_POWER_SERVICE,
                            Icon(Icons.flash_on))));
              },
            ),
            ListTile(
              leading: Icon(Icons.clear),
              title: Text(Texts.reset()),
              onLongPress: () {
                _clearAll();
              },
            ),
          ],
        ));
  }

  void _clearAll() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('pulse_device', "");
    prefs.setString('cadence_device', "");
    prefs.setString('speed_device', "");
    prefs.setString('power_device', "");

    setState(() {
      _pulseName = Texts.notAssigned();
      _cadenceName = Texts.notAssigned();
      _powerName = Texts.notAssigned();
    });
  }
}
