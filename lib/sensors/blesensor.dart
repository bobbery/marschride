// @dart=2.9
import 'dart:async';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:flutter_blue/gen/flutterblue.pb.dart' as protos;
import 'package:shared_preferences/shared_preferences.dart';

import 'gattconstants.dart';
import 'globalsensordata.dart';

class BleSensor {
  final String _sensorId;
  StreamSubscription<List<int>> _valSubscr;
  StreamSubscription<List<int>> _batSubscr;
  String _serviceUid = UUID_HEART_RATE_SERVICE;
  String _charUid = UUID_HEART_RATE_MEASUREMENT_CHAR;
  BluetoothDevice _device;

  BluetoothCharacteristic charValue;
  BluetoothCharacteristic charBattery;

  final _valueStreamController = StreamController<int>.broadcast();
  final _batteryStreamController = StreamController<int>.broadcast();
  final _statusStreamController =
      StreamController<BluetoothDeviceState>.broadcast();

  bool _isConfigured = false;
  int _lastCrankRev = 0;
  int _noCandenceCounter = 0;

  Stream<int> get value => _valueStreamController.stream;
  Stream<int> get battery => _batteryStreamController.stream;
  Stream<BluetoothDeviceState> get status => _statusStreamController.stream;
  bool get isConfigured => _isConfigured;

  void reconnect() async {
    _valSubscr?.cancel();
    _batSubscr?.cancel();
    charValue = null;
    charBattery = null;
    try {
      await _device.disconnect();
    } catch (_) {
      print('Not connected');
    }
    if (_sensorId == 'power') {
      _serviceUid = UUID_CYCLING_POWER_SERVICE;
      _charUid = UUID_CYCLING_POWER_MEASUREMENT_CHAR;
    }

    if (_sensorId != '') {
      _loadState(this._sensorId).then((value) {
        if (value != null) {
          _isConfigured = true;
          _connectToDevice(value);
        }
      });
    }
  }

  BleSensor(this._sensorId) {
    if (_sensorId == 'power') {
      _serviceUid = UUID_CYCLING_POWER_SERVICE;
      _charUid = UUID_CYCLING_POWER_MEASUREMENT_CHAR;
    }
    if (_sensorId == 'cadence') {
      _serviceUid = UUID_CYCLING_SPEED_AND_CADENCE;
      _charUid = UUID_CSC_MEASUREMENT_CHAR;
    }

    if (_sensorId != '') {
      _loadState(this._sensorId).then((device) {
        if (device != null) {
          _isConfigured = true;

          device.state.listen((state) {
            _statusStreamController.add(state);
          });
          _connectToDevice(device);
        }
      });
    }
  }

  Future<BluetoothDevice> _loadState(String id) async {
    final prefs = await SharedPreferences.getInstance();
    String deviceData = prefs.getString(id + '_device');
    if (deviceData != null && deviceData != "") {
      _device = BluetoothDevice.fromProto(
          protos.BluetoothDevice.fromJson(deviceData));
      return _device;
    }
    return null;
  }

  void connectValueChar() async {
    print(_sensorId.toUpperCase() + ' CONNECTED !!!');
    _valSubscr = charValue.value.listen((value) {
      int val = 0;
      if (_sensorId == 'pulse') {
        if (value.length > 1) {
          val = value[1];
          print("new pulseq: $val");
          GlobalSensorData.heartRate = val;
        }
      }
      if (_sensorId == 'power') {
        if (value.length > 5) {
          //val = value[7] * 256 + value[6];
          val = value[3] * 256 + value[2];
          GlobalSensorData.power = val;
        }
      }
      if (_sensorId == 'cadence') {
        if (value.length > 2) {
          // extract crank revolution
          if (value[0] == 2) {
            // cadence only
            val = value[4] * 256 + value[3];
          } else if (value[0] == 3) {
            // cadence and speed
            val = value[10] * 256 + value[9];
          }
          print("new cranc rev: $val");
          if ((val - _lastCrankRev) <= 0) {
            _lastCrankRev = val;
            _noCandenceCounter++;
            if (_noCandenceCounter > 25) {
              _valueStreamController.add(0);
              _noCandenceCounter = 0;
            }
            return;
          }
          double rpm = 60 / ((val - _lastCrankRev) / 1024);
          _lastCrankRev = val;
          val = rpm.toInt();
          GlobalSensorData.cadence = val;
          print("new cadence: $val");
        }
      }
      _valueStreamController.add(val);
    });

    await charValue.setNotifyValue(true);
  }

  void connectBatteryChar() async {
    print(_sensorId.toUpperCase() + ' BATTERY CONNECTED !!!');
    _batteryStreamController.add(100);

    _batSubscr = charBattery.value.listen((value) {
      if (value.length > 0) {
        _batteryStreamController.add(value[0]);
        print(_sensorId + " bat: ${value[0]}");
      }
    });
  }

  void _connectToDevice(BluetoothDevice device) async {
    await _valSubscr?.cancel();
    await _batSubscr?.cancel();

    charValue = null;
    charBattery = null;

    int counter = 0;
    await device.disconnect();

    while (true) {
      BluetoothDeviceState state = await device.state.first;
      if (state == BluetoothDeviceState.disconnected) {
        break;
      }
      counter++;
      if (counter > 10) {
        print("GIVE UP (Disconnect)");
        return;
      }
    }

    await device.connect(autoConnect: false);

    counter = 0;
    while (true) {
      BluetoothDeviceState state = await device.state.first;
      if (state == BluetoothDeviceState.connected) {
        break;
      }
      counter++;
      if (counter > 10) {
        print("GIVE UP (Connect)");
        return;
      }
    }

    List<BluetoothService> services = await device.discoverServices();

    services.forEach((service) {
      if (service.uuid.toString() == _serviceUid) {
        service.characteristics.forEach((c) {
          if (c.uuid.toString() == _charUid) {
            charValue = c;
          }
        });
      }
      if (service.uuid.toString() == UUID_BATTERY_SERVICE) {
        service.characteristics.forEach((c) {
          if (c.uuid.toString() == UUID_BATTERY_LEVEL_CHAR) {
            charBattery = c;
          }
        });
      }
    });
    if (charBattery != null) {
      connectBatteryChar();
      await charBattery.setNotifyValue(true);
    }
    if (charValue != null) {
      connectValueChar();
      try {
        await charValue.setNotifyValue(true);
      } catch (err) {}
    }
  }

  void dispose() async {
    await _valueStreamController?.close();
    await _batteryStreamController?.close();
    await _valSubscr?.cancel();
    await _batSubscr?.cancel();
    charValue = null;
    charBattery = null;
    await _device.disconnect();
  }
}
