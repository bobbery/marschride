// @dart=2.9
import 'dart:async';
import 'dart:io';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';
import 'package:path_provider/path_provider.dart';
import 'package:dart_numerics/dart_numerics.dart' as numerics;
import 'package:flutter_map_location/flutter_map_location.dart';

import 'toleranttileprovider.dart';
import 'texts.dart';

int long2tilex(double lon, int z) {
  return ((lon + 180.0) / 360.0 * (1 << z)).floor();
}

int lat2tiley(double lat, int z) {
  double latrad = lat * pi / 180.0;
  return ((1.0 - numerics.asinh(tan(latrad)) / pi) / 2.0 * (1 << z)).floor();
}

class MapLoaderPage extends StatefulWidget {
  final LatLng _curPos;

  MapLoaderPage(LatLng curPos) : _curPos = curPos;

  @override
  _MapLoaderPageState createState() => _MapLoaderPageState();
}

class _MapLoaderPageState extends State<MapLoaderPage> {
  String _offlineMapPath;
  MapController mapController = MapController();
  double _zoom = 16;
  bool _visualizeDownloadedMap = false;
  int _numTiles = 0;
  int _numProcessedTiles = 0;
  bool _internetConnection = false;
  StreamSubscription _mcEvents;
  final List<Marker> userLocationMarkers = <Marker>[];
  bool _tooMuchTiles = false;

  void _findOfflineMapPath() async {
    final Directory extDir = await getApplicationDocumentsDirectory();
    if (!mounted) return;
    setState(() {
      _offlineMapPath = extDir.path + "/offlineMap";
      print(_offlineMapPath);
    });
  }

  void _checkConnectivity() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print('connected');
        _internetConnection = true;
      }
    } on SocketException catch (_) {
      print('not connected');
      _internetConnection = false;
    }
  }

  @override
  void initState() {
    super.initState();
    mapController = MapController();
    _findOfflineMapPath();
    _checkConnectivity();
    _mcEvents = mapController.mapEventStream.listen((event) {
      _calcSelectedTiles();
      setState(() {});
    });
  }

  Future _loadTile(int x, int y) async {
    int z = 16;
    HttpClient client = HttpClient();

    List<int> _downloadData = [];
    File(_offlineMapPath +
            "/${z.toString()}/${x.toString()}/${y.toString()}.png")
        .createSync(recursive: true);
    var fileSave = File(_offlineMapPath +
        "/${z.toString()}/${x.toString()}/${y.toString()}.png");

    HttpClientRequest request = await client.getUrl(Uri.parse(
        "https://a.tile.openstreetmap.org/${z.toString()}/${x.toString()}/${y.toString()}.png"));

    HttpClientResponse response = await request.close();

    response.listen((d) => _downloadData.addAll(d), onDone: () {
      fileSave.writeAsBytes(_downloadData);
      setState(() {
        _numProcessedTiles++;
      });
    });
  }

  void _calcSelectedTiles() {
    int minX = min(long2tilex(mapController.bounds.east, 16),
        long2tilex(mapController.bounds.west, 16));
    int maxX = max(long2tilex(mapController.bounds.east, 16),
        long2tilex(mapController.bounds.west, 16));
    int minY = min(lat2tiley(mapController.bounds.south, 16),
        lat2tiley(mapController.bounds.north, 16));
    int maxY = max(lat2tiley(mapController.bounds.south, 16),
        lat2tiley(mapController.bounds.north, 16));

    print("minX: ${minX.toString()}");
    print("maxX: ${maxX.toString()}");
    print("minY: ${minY.toString()}");
    print("maxY: ${maxY.toString()}");

    _numTiles = 0;
    for (int x = minX; x <= maxX; x++) {
      for (int y = minY; y <= maxY; y++) {
        _numTiles++;
      }
    }
    setState(() {});
  }

  void _loadSelectedTiles() async {
    int minX = min(long2tilex(mapController.bounds.east, 16),
        long2tilex(mapController.bounds.west, 16));
    int maxX = max(long2tilex(mapController.bounds.east, 16),
        long2tilex(mapController.bounds.west, 16));
    int minY = min(lat2tiley(mapController.bounds.south, 16),
        lat2tiley(mapController.bounds.north, 16));
    int maxY = max(lat2tiley(mapController.bounds.south, 16),
        lat2tiley(mapController.bounds.north, 16));

    print("minX: ${minX.toString()}");
    print("maxX: ${maxX.toString()}");
    print("minY: ${minY.toString()}");
    print("maxY: ${maxY.toString()}");

    _numTiles = 0;
    for (int x = minX; x <= maxX; x++) {
      for (int y = minY; y <= maxY; y++) {
        _numTiles++;
      }
    }

    print("numTiles: ${_numTiles.toString()}");

    if (_numTiles > 1000) {
      setState(() {
        _tooMuchTiles = true;
      });
      return;
    }

    _numProcessedTiles = 0;
    for (int x = minX; x <= maxX; x++) {
      for (int y = minY; y <= maxY; y++) {
        await _loadTile(x, y);
      }
    }
  }

  void _onItemTapped(int index) async {
    print("Tap index ${index.toString()}");
    if (index == 0) {
      setState(() {
        mapController.move(widget._curPos, _zoom.toDouble());
      });
    }
    if (index == 1) {
      if (_internetConnection) {
        _loadSelectedTiles();
      }
    }
    if (index == 2) {
      setState(() {
        _visualizeDownloadedMap = !_visualizeDownloadedMap;
        print("Visualisation ${_visualizeDownloadedMap.toString()}");
        mapController.move(mapController.center, 16.0);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.black),
            onPressed: () => Navigator.pop(context)),
        title: Text(_numTiles.toString() + " " + Texts.tiles()),
      ),
      body: Stack(
        alignment: const Alignment(0.6, 0.6),
        children: [
          _numProcessedTiles > 0
              ? Container(
                  color: Colors.lightGreen,
                  child: Text(""),
                )
              : Container(
                  child: buildMap(context),
                ),
          Column(
            children: [
              Spacer(),
              Container(
                child: Text("© OpenStreetMap Contributors"),
              ),
            ],
          ),
          if (_tooMuchTiles)
            AlertDialog(
              title: Text(Texts.tooManyTiles()),
              content: Text(
                  "${_numTiles.toString()} " + Texts.tooManyTilesSelected()),
              actions: [
                ElevatedButton(
                  child: Text("Okay"),
                  onPressed: () {
                    setState(() {
                      _numTiles = 0;
                      _numProcessedTiles = 0;
                      _tooMuchTiles = false;
                    });
                    Navigator.pop(context);
                    Navigator.pop(context);
                    setState(() {});
                  },
                ),
              ],
            ),
          if (_numProcessedTiles > 0)
            AlertDialog(
              title: Text(Texts.mapLoad()),
              content: Text(Texts.loadTile() +
                  " ${_numProcessedTiles.toString()} " +
                  Texts.of() +
                  " ${_numTiles.toString()}"),
              actions: [
                if (_numProcessedTiles == _numTiles)
                  ElevatedButton(
                    child: Text("Okay"),
                    onPressed: () {
                      setState(() {
                        _numTiles = 0;
                        //_numProcessedTiles = 0;
                      });
                      Navigator.pop(context);
                      Navigator.pop(context);
                    },
                  ),
              ],
            ),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.lightGreen,
        selectedItemColor: Colors.black,
        unselectedItemColor: Colors.black,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(
              Icons.my_location,
              color: Colors.black,
            ),
            label: Texts.position(),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.cloud_download,
              color: Colors.black,
            ),
            label: Texts.load(),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.remove_red_eye,
              color: _visualizeDownloadedMap ? Colors.red : Colors.black,
            ),
            label: Texts.show(),
          ),
        ],
        onTap: _onItemTapped,
      ),
    );
  }

  Widget buildMap(BuildContext context) {
    TileLayerOptions layer = TileLayerOptions(
        urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
        subdomains: ['a', 'b', 'c']);
    if (_visualizeDownloadedMap) {
      layer = TileLayerOptions(
        tileProvider: TolerantFileTileProvider(),
        urlTemplate: _offlineMapPath + "/{z}/{x}/{y}.png",
      );
    }

    double circleRadius = 0.0;
    LatLng circlePos = LatLng(0.0, 0.0);

    if (!_visualizeDownloadedMap) {
      try {
        Distance dist = Distance();
        circleRadius = dist.as(LengthUnit.Meter, mapController.bounds.northEast,
                mapController.bounds.northWest) /
            2.0 *
            0.95;
        circlePos = mapController.center;
        print("Circle Radius ${circleRadius.toString()}");
      } catch (e) {}
    }

    return FlutterMap(
      mapController: mapController,
      options: MapOptions(
        maxZoom: 18,
        plugins: <MapPlugin>[
          LocationPlugin(),
        ],
        interactiveFlags: _visualizeDownloadedMap
            ? InteractiveFlag.drag
            : InteractiveFlag.all & ~InteractiveFlag.rotate,
      ),
      layers: <LayerOptions>[
        layer,
        MarkerLayerOptions(markers: userLocationMarkers),
        LocationOptions(
          (BuildContext context, ValueNotifier<LocationServiceStatus> status,
              Function onPressed) {
            return Text("");
          },
          markerBuilder: myMarkerBuilder,
          onLocationRequested: (LatLngData ld) {
            if (ld == null || ld.location == null) {
              return;
            }
            setState(() {
              print('Location requested MapLoader: ${ld?.location}');
              mapController?.move(ld.location, 16.0);
            });
          },
        ),
        CircleLayerOptions(
          circles: [
            new CircleMarker(
              point: circlePos,
              color: Colors.transparent,
              borderColor:
                  !_visualizeDownloadedMap ? Colors.red : Colors.transparent,
              borderStrokeWidth: 3.0,
              radius: circleRadius,
              useRadiusInMeter: true,
            ),
          ],
        ),
      ],
    );
  }

  @override
  void dispose() {
    _mcEvents?.cancel();
    super.dispose();
  }
}
