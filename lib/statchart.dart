// @dart=2.9
import 'dart:math';

import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

import 'texts.dart';

class StatChart extends StatefulWidget {
  final String _title = "Diagramme";
  final List<Map<String, Object>> data;

  StatChart(this.data);

  @override
  _StatChartState createState() => _StatChartState();
}

class _StatChartState extends State<StatChart> {
  List<Color> gradientColors = [Colors.green];
  double _minY = double.infinity;
  double _maxY = double.negativeInfinity;
  double _midY = 0.0;
  int _valKeyIndex = 0;
  final List<String> _valKeys = ['power', 'speed', 'cadence', 'pulse', 'ele'];

  List<FlSpot> _spots;

  @override
  void initState() {
    _spots = getSpots();
    if (_spots.isEmpty) {
      _minY = 0.0;
      _maxY = 0.0;
      _midY = 0.0;
    }
    super.initState();
  }

  List<FlSpot> getSpots() {
    List<FlSpot> spots = [];
    int count = 0;

    for (Map<String, Object> line in widget.data) {
      double val = double.parse(line[_valKeys[_valKeyIndex]].toString());
      _minY = min(_minY, val);
      _maxY = max(_maxY, val);
      _midY += val;

      spots.add(FlSpot(count.toDouble(), val));
      count++;
    }
    _midY /= count;
    return spots;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.black),
            onPressed: () => Navigator.pop(context)),
        title: Text(widget._title),
      ),
      body: Flex(direction: Axis.vertical, children: [
        Spacer(),
        Text(Texts.chartHeadings()[_valKeyIndex],
            style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold)),
        Spacer(),
        Container(height: 300, child: LineChart(mainData())),
        Spacer(),
        Card(
            color: Colors.lightGreen[100],
            child: ListTile(
                leading: Icon(Icons.loop),
                title: Text(Texts.avgVal()),
                trailing: Text("${_midY.round()}",
                    style:
                        TextStyle(fontSize: 32, fontWeight: FontWeight.bold)))),
        Card(
            color: Colors.lightGreen[100],
            shadowColor: Colors.green,
            child: ListTile(
                leading: Icon(Icons.arrow_upward),
                title: Text(Texts.max()),
                trailing: Text("${_maxY.round()}",
                    style:
                        TextStyle(fontSize: 32, fontWeight: FontWeight.bold)))),
        Card(
            color: Colors.lightGreen[100],
            child: ListTile(
                leading: Icon(Icons.arrow_downward),
                title: Text(Texts.min()),
                trailing: Text("${_minY.round()}",
                    style:
                        TextStyle(fontSize: 32, fontWeight: FontWeight.bold)))),
        Spacer(),
      ]),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.lightGreen,
        selectedItemColor: Colors.black,
        unselectedItemColor: Colors.black,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(
              Icons.arrow_left,
              color: Colors.black,
              size: 40,
            ),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.show_chart,
              color: Colors.black,
            ),
            label: Texts.chartHeadings()[_valKeyIndex],
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.arrow_right,
              color: Colors.black,
              size: 40,
            ),
            label: '',
          ),
        ],
        onTap: _onItemTapped,
      ),
    );
  }

  void _onItemTapped(int index) async {
    if (index == 0 && _valKeyIndex > 0) {
      _valKeyIndex--;
    }
    if (index == 2 && _valKeyIndex < _valKeys.length - 1) {
      _valKeyIndex++;
    }
    setState(() {
      _minY = double.infinity;
      _maxY = double.negativeInfinity;
      _midY = 0.0;
      _spots = getSpots();

      if (_spots.isEmpty) {
        _minY = 0.0;
        _maxY = 0.0;
        _midY = 0.0;
      }
    });
  }

  LineChartData mainData() {
    return LineChartData(
      gridData: FlGridData(
        show: true,
        drawVerticalLine: true,
        getDrawingHorizontalLine: (value) {
          return FlLine(
            color: const Color(0xff37434d),
            strokeWidth: 1,
          );
        },
        getDrawingVerticalLine: (value) {
          return FlLine(
            color: const Color(0xff37434d),
            strokeWidth: 1,
          );
        },
      ),
      titlesData: FlTitlesData(
        show: true,
        bottomTitles: SideTitles(
          showTitles: false,
          reservedSize: 22,
          getTextStyles: (value, dbl) => const TextStyle(
              color: Colors.black, fontWeight: FontWeight.bold, fontSize: 16),
          getTitles: (value) {
            return '';
          },
          margin: 8,
        ),
        leftTitles: SideTitles(
          showTitles: false,
          getTextStyles: (value, dbl) => const TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,
            fontSize: 15,
          ),
          getTitles: (value) {
            return '';
          },
          reservedSize: 28,
          margin: 12,
        ),
      ),
      borderData: FlBorderData(
          show: true,
          border: Border.all(color: const Color(0xff37434d), width: 1)),
      minX: 0,
      maxX: widget.data.length.toDouble(),
      minY: _minY,
      maxY: _maxY,
      lineBarsData: [
        LineChartBarData(
          spots: _spots,
          isCurved: false,
          colors: gradientColors,
          barWidth: 3,
          isStrokeCapRound: true,
          dotData: FlDotData(
            show: false,
          ),
          belowBarData: BarAreaData(
            show: true,
            colors:
                gradientColors.map((color) => color.withOpacity(0.3)).toList(),
          ),
        ),
      ],
    );
  }
}
